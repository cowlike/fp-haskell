-- 1
f1 :: Integral a => a -> a -> a -> Bool
f1 m n p = div m n >= p

-- 2
g1 :: Integral a => a -> Bool -> a -> Bool
g1 a b x = (mod a x > 0) && b

-- 3
myOr :: Bool -> Bool -> Bool
myOr b1 b2 = False
myOr True b2 = True
myOr b1 True = True

-- 4
f :: Int -> Int
f n = g n (n+1)


g :: Int -> Int -> Int
g m i
  | (mod i m) == 0 = i
  | otherwise = g m (i+1)

-- 5
h1 :: Integral a => a -> a -> a
h1 m 0 = m
h1 m n = h (div m 10) (10*n + (mod m 10))

-- 6
-- What is the value of h 13267 0, where the definition of h is given below?
h :: Integral a => a -> a -> a
h m 0 = m
h m n = h (div m 10) (10*n + (mod m 10))
