-- 1
f1 xs = g1 (0,xs)

g1 (m,[]) = m
g1 (m,x:xs) = g1 (m+1,xs)

-- 2
f2 xs = g2 (0,xs)

g2 (m,[]) = m
g2 (m,x:xs) = g2 (m+1,xs)

-- 3
h :: Ord a => [a] -> [Bool]
h [] = []
h [x] = []
h (x:y:ys) = (x <= y): (h (y:ys))

-- 4
--h :: Ord a => [a] -> [Bool]
h [] = []
h [x] = []
h (x:y:ys) = (x <= y): (h (y:ys))

-- 5
--h :: Ord a => [a] -> [Bool]
h [] = []
h [x] = []
h (x:y:ys) = (x <= y): (h (y:ys))

-- 6
--h :: Ord a => [a] -> [Bool]
h [] = []
h [x] = []
h (x:y:ys) = (x <= y): (h (y:ys))

-- 7
{-- Given that the expression []:xs is well typed, which of the following is not a possible type assignment for xs?
 xs :: [Int]
 xs :: [[Float]]
 xs :: [[[Char]]]
 xs :: [[[[Bool]]]] --}

-- 8
g :: [a] -> [a] -> [a]
g ys [] = ys
g ys [m] = m:ys
g ys (z:zs) = g (ys ++ [z]) zs
