import Data.List

-- 1
repl :: String -> String
repl "" = ""
repl (x:xs) = x : x : (repl xs)

repl1 :: String -> String
repl1 = foldr (\v t -> v:v:t) []

-- 2
remDup :: [Int] -> [Int]
remDup [] = []
remDup [x] = [x]
remDup (x:xs) = x : remDup (filter (/= x) xs)

-- 3
remChamp :: [Int] -> [Int]
remChamp xs
  | length xs < 2 = []
  | otherwise = takeWhile (/= champ) xs ++
      tail (dropWhile (/= champ) xs)
    where champ = maximum xs

-- 4

remRunnerUp :: [Int] -> [Int]
remRunnerUp xs
  | length xs < 2 = xs
  | otherwise = takeWhile (/= runnerUp) xs ++
      tail (dropWhile (/= runnerUp) xs)
    where runnerUp = (head.drop 1.sortBy (flip compare)) xs
