import Data.List

-- 1
-- ramanujan :: Int -> Int
ramanujan n = [x*x*x + y*y*y
  | x <- [1..], y <- [1..x], a <- [y+1..x],
    b <- [a..x], x*x*x + y*y*y == a*a*a + b*b*b ] !! (n-1)

-- 2a
is_matrix :: [[a]] -> Bool
is_matrix [] = False
is_matrix [[]] = False
is_matrix (x:y:xs) = (length x) == (length y) && is_matrix (y:xs)
is_matrix _ = True

-- is_matrix [] = False
-- is_matrix [[],[],[]] = False
-- is_matrix [[2,3], [4,5], [6,7]] = True
-- is_matrix [[2,3,4,5,6,7]] = True

-- 2b
is_square_matrix :: [[a]] -> Bool
is_square_matrix xs = is_matrix xs && length xs == length (head xs)

-- is_square_matrix [] = False
-- is_square_matrix [[],[],[]] = False
-- is_square_matrix [[1]] = True
-- is_square_matrix [[1,2,3], [4,5,6], [7,8,9]] = True

-- 2c
addable :: [[Int]] -> [[Int]] -> Bool
addable xs ys = is_matrix xs && is_matrix ys &&
  lhead xs == lhead ys
    where lhead = length.head

-- addable [[1,2],[3,4]] [[1,2,3],[4,5,6]] = False
-- addable [[1,2],[3,4]] [[5,6],[7,8]] = True

-- 2d
add_lists :: Num a => [a] -> [a] -> [a]
add_lists [] _ = []
add_lists _ [] = []
add_lists (x:xs) (y:ys) = (x + y) : add_lists xs ys

add_matrix :: [[Int]] -> [[Int]] -> [[Int]]
add_matrix xs ys
  | not (addable xs ys) = []
  | otherwise = addAllTheThings xs ys
    where
      addAllTheThings [] _ = []
      addAllTheThings _ [] = []
      addAllTheThings (x:xs) (y:ys) = add_lists x y : addAllTheThings xs ys

-- add_matrix [[1,2,3,4]] [[5,6,7,8]] = [[6,8,10,12]]
-- add_matrix [[1,2],[3,4]] [[5,6],[7,8]] = [[6,8],[10,12]]

-- 2e
multiplyable :: [[Int]] -> [[Int]] -> Bool
multiplyable xs ys = is_matrix xs && is_matrix ys &&
  (length.head) xs == length ys

-- multiplyable [[1,2,3],[4,5,6]] [[1,2],[3,4]] = False
-- multiplyable [[1,2],[3,4]] [[1,2,3],[4,5,6]] = True

-- 2f
-- Auxilliary function for row multiplication
dotProduct :: [Int] -> [Int] -> Int
dotProduct xs ys = sum ( zipWith (*) xs ys )

-- Multiplication of Marices
multiply_matrix :: [[Int]] -> [[Int]] -> [[Int]]
multiply_matrix m n
  | multiplyable m n = [ map (dotProduct r) (transpose n) | r <- m ]
  | otherwise = []


-- multiply_matrix [[1,2],[3,4]] [[1,2,3],[4,5,6]] = [[9,12,15],[19,26,33]]
-- multiply_matrix [[1,2,3],[4,5,6]] [[1,2],[3,4],[5,6]] = [[22,28],[49,64]]
